module Homework.D03_RecursionPatternPolyPrelude.Golf where
import Safe
import Data.Maybe
-- extract every Nth element in list
skip :: [t] -> Int -> [t]
skip l n
  | n < 2 = l -- if N is to small, keep list unchanged
  | length l < n = [] -- if N is to big, return empty list
  | otherwise = -- if N is fine :
      let
        x = l !! (n-1) -- get Nth element
        xli = drop n l -- remove Nth first element in list...
        xl = skip xli n -- ... and continue skip Nth element in remaining list
      in (x:xl)
-- The output of skips is a list of lists
-- The first list in the output should be the same as the input list
-- The second list in the output should contain every second element from the input list
-- ... and the nth list in the output should contain every nth element from the input list
skips :: [t] -> [[t]]
skips l = map (skip l) [1..length l] -- dispatch skipping : we will skip from 1 to Nth at once
-- get local maximum
localMaximum :: Ord t => (t,t,t) -> Maybe t
localMaximum (a,b,c)
    | a<b && b>c = Just b
    | otherwise = Nothing
-- split list to triplets
triplets :: [t] -> [(t,t,t)]
triplets l
  | length l < 3 =  []
  | otherwise =
  let
    a = l !! 0
    b = l !! 1
    c = l !! 2
    sl = drop 3 l
  in (a,b,c) : triplets (b:c:sl)
-- A local maximum of a list is an element of the list which is strictly greater than both the elements immediately before and after it
-- For example, in the list[2,3,4,1,5], the only local maximum is 4, since it is greater than the elements immediately before and after it (3 and1 )
-- 5 is not a local maximum since there is no element that comes after it
localMaxima :: Ord t => [t] -> [t]
localMaxima = catMaybes . -- keep only Just elements
    map localMaximum . -- apply localMaximum to all elements
      triplets -- split list to triplets
-- an histogram is the appearance number of each digits (from 0 to 9 included)
type Histogram = (Int,Int,Int,Int,Int,Int,Int,Int,Int,Int)
-- print line # n of an histogram
-- ie : print a '*' when digit appear at least n times, ' ' otherwise
printLine :: Histogram -> Int -> String
printLine (c0,c1,c2,c3,c4,c5,c6,c7,c8,c9) n =
  let printStar c n = if c>n then '*' else ' ' -- print star only when count match
  in [
    printStar c0 n -- print '*' for digit 0
    ,printStar c1 n -- print '*' for digit 1
    ,printStar c2 n --  -- print '*' for digit ...
    ,printStar c3 n
    ,printStar c4 n
    ,printStar c5 n
    ,printStar c6 n
    ,printStar c7 n
    ,printStar c8 n
    ,printStar c9 n
    , '\n' -- end of line
  ]
-- give histogram maximum number of lines
ceil :: Histogram -> Int
ceil (c0,c1,c2,c3,c4,c5,c6,c7,c8,c9) = maximum [c0,c1,c2,c3,c4,c5,c6,c7,c8,c9] - 1
-- print histogram
printHistogram :: Histogram -> String
printHistogram h = concat ( -- merge all lines
    map (printLine h) $ -- print each line ...
      reverse [0..ceil h] -- ... from 9 to 0
  )++(
    take 9 (repeat '=')++"\n"++['1'..'9']++"\n" -- footer
  )
-- cont element in a list
count :: Eq t => [t] -> t -> Int
count l n = length $ filter (n==) l
-- make an histogram from a list
toHistogram :: [Int] -> Histogram
toHistogram l = (count l 0,count l 1,count l 2,count l 3,count l 4,count l 5,count l 6,count l 7,count l 8,count l 9)
-- takes as input a list of Integers between 0 and 9 (inclusive),and outputs a vertical histogram showing how many of each number were in the input list
histogram :: [Int] -> String
histogram l = printHistogram $ toHistogram l
-- putStr $ histogram [1,1,1,5]
-- putStr $ histogram [1,4,5,4,6,6,3,4,2,4,9]
