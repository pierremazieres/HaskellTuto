module Homework.D02_AlgebraicDataTypes.LogAnalysis where
import Homework.D02_AlgebraicDataTypes.Log
-- parse individual message
parseMessage :: String -> LogMessage
parseMessage rawLog = case words rawLog of
    "I":timestamp:message             -> LogMessage Info (read timestamp :: TimeStamp) $ unwords message
    "W":timestamp:message             -> LogMessage Warning (read timestamp :: TimeStamp) $ unwords message
    "E":errorNumber:timestamp:message -> LogMessage (Error (read errorNumber :: Int)) (read timestamp :: TimeStamp) $ unwords message
    _                                 -> Unknown rawLog
-- parse whole log file
parse :: String -> [LogMessage]
parse rawFile = map parseMessage $ lines rawFile
-- insert new log message in message tree
insert :: LogMessage -> MessageTree -> MessageTree
insert logMessage Leaf              = Node Leaf logMessage Leaf
insert newLogMessage@(LogMessage _ newTimeStamp _) (Node leftNode rootLogMessage@(LogMessage _ rootTimeStamp _) rightNode)
    | newTimeStamp <= rootTimeStamp = Node (insert newLogMessage leftNode) rootLogMessage rightNode
    | otherwise                     = Node leftNode rootLogMessage $ insert newLogMessage rightNode
insert _ _                          = Leaf
-- build a tree from messages list
build :: [LogMessage] -> MessageTree
build (logMessage:[]) = insert logMessage Leaf
build (firstLogMessage:otherLogMessages) = insert firstLogMessage $ build otherLogMessages
build _ = Leaf
-- order messages
inOrder :: MessageTree -> [LogMessage]
inOrder (Node leftTree (Unknown _) rightTree) = (inOrder leftTree)++(inOrder rightTree)
inOrder (Node Leaf logMessage Leaf) = [logMessage]
inOrder (Node leftTree logMessage rightTree) = (inOrder leftTree)++[logMessage]++(inOrder rightTree)
inOrder Leaf = []
-- filter only relevant messages
relevantErrorLevel = 50
whatWentWrong :: [LogMessage] -> [String]
whatWentWrong ((LogMessage (Error severity) _ message):otherLogMessages)
    | severity >= relevantErrorLevel = message : whatWentWrong otherLogMessages
    | otherwise = whatWentWrong otherLogMessages
whatWentWrong (_:otherLogMessages) = whatWentWrong otherLogMessages
whatWentWrong _ = []
-- final test command
-- testWhatWentWrong parse whatWentWrong "src/Homework/D02_AlgebraicDataTypes/error.log"
