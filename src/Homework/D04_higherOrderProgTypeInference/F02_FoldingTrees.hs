module Homework.D04_higherOrderProgTypeInference.F02_FoldingTrees where
-- a tree with his height
-- the height of a tree with a single node_ is 0
-- the height of a tree with three nodes, whose root has two children, is 1
-- and so on
data Tree a = Leaf
  | Node{
    height::Integer,
    left::Tree a,
    element::a,
    right::Tree a
  }
  deriving (Show, Eq)
heightLeft = height.left
heightRight = height.right
-- is tree balanced ?
-- a binary tree is balanced if
-- - the height of its left and right subtrees differ by no more than 1
-- - and its left and right subtrees are also balanced
balanced :: Tree a -> Bool
balanced Leaf = True
balanced (Node _ Leaf _ Leaf) = True
balanced (Node _ l _ Leaf) = height l == 0
balanced (Node _ Leaf _ r) = height r == 0
balanced t =
    abs( heightLeft t - heightRight t ) <= 1
    && balancedLeft t && balancedRight t
balancedLeft = balanced.left
balancedRight = balanced.right
-- shortest length
shortestHeight :: Tree a -> Integer
shortestHeight Leaf = 0
shortestHeight t = 1 + min (shortestHeightLeft  t) (shortestHeightRight t)
shortestHeightLeft = shortestHeight.left
shortestHeightRight = shortestHeight.right
-- is tree fully filled ?
-- a tree is fully filled when lesser & higher branches are equal
fullyFilled :: Tree a -> Bool
fullyFilled Leaf = True
fullyFilled (Node _ Leaf _ Leaf) = True
fullyFilled (Node _ _ _ Leaf) = False
fullyFilled (Node _ Leaf _ _) = False
fullyFilled t =
  let shortestHeightRightT = shortestHeightRight t
  in
    shortestHeightRightT == shortestHeightLeft t
    && shortestHeightRightT == heightRight t
-- add an element to a tree
addElem :: a -> Tree a -> Tree a
addElem e Leaf = Node 0 Leaf e Leaf
addElem e t
  | ( shortestHeightRight t <= shortestHeightLeft t ) =
    Node
    ( if fullyFilled t then h+1 else h )
    l e $ addElemT r
  | otherwise = Node h r e $ addElemT l
  where
    r = right t
    l = left t
    h = height t
    addElemT = addElem $ element t
-- fold a list to a tree
foldTree :: [a] -> Tree a
foldTree = foldr addElem Leaf
