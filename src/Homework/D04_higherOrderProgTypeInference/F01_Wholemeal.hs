module Homework.D04_higherOrderProgTypeInference.F01_Wholemeal where
-- original functions
fun1 :: [Integer] -> Integer
fun1 []     = 1
fun1 (x:xs)
    | even x    = (x - 2) * fun1 xs
    | otherwise = fun1 xs
fun2 :: Integer -> Integer
fun2 1 = 0
fun2 n | even n    = n + fun2 (n `div` 2)
       | otherwise = fun2 (3 * n + 1)
-- new functions
-- 1. keep only even numbers : filter even
-- 2. apply minus 2 : map (\x -> x-2)
-- 3. multiply all numbers : product
fun1' :: [Integer] -> Integer
fun1' = product . map (\x -> x-2) . filter even
fun2' :: Integer -> Integer
fun2' n = foldr
  -- 2. compute from fun2(n), using fun2(p), ... using fun2(1)
  -- for given n and fun2(n-1), return fun2(n)
  -- example :
  -- - 8 is even, merge 8 fun2(4) = merge 8 6 = 8+6 = 14
  -- - 3 is odd, merge 3 fun2(10) = merge 8 40 = 40
  (\n fp -> case () of
    _ | n < 2 -> 0
      | even n -> n + fp
      | odd n -> fp
  )
  0 $ takeWhile (>0) $ iterate
  -- 1. compute all needed indices : [ n , ... , p , ... , 1 ]
  -- for a given an index, return previous needed one
  -- example :
  -- - previous 1 need nothing
  -- - 6 is even, so previous(6) = 6/2 = 3
  -- - 5 is odd, so previous(5) = 3*5+1 = 16
  (\n -> case () of
    _ | even n -> div n 2
      | odd n -> 3 * n + 1
  )
  n
