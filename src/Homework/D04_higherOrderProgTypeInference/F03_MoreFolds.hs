module Homework.D04_higherOrderProgTypeInference.F03_MoreFolds where
-- xor
-- 0. set accumulator to False
-- 1. keep only True values
-- 2. 1st True is different than accumulator (ie. False), accumulator becomes True
-- 3. 2nd True is equals than accumulator (ie. True), accumulator becomes False
-- 4. for every other odd True (3th, 5th, ...), see §2
-- 5. for other even True (4th, 6th, ...), see §3
xor :: [Bool] -> Bool
xor bs = foldr (\x y->x/=y) False $ filter (True&&) bs
-- map
-- 1. `:` is concatenation function
--    it takes an element `e` and a list [e0 e1 ...] to return [e e0 e1 ...]
-- 2. so `(:).f` takes an element `e::a` and a list `[e0 e1 ...]::[b]` to return `[(f e) e0 e1 ...]::[b]`
-- 3. so `foldr ((:).f) [] [e]` returns `f e:[]`
--    and `foldr ((:).f) [] [... e1 e0]` returns `f e0:foldr ((:).f) [] [... e1]`
map' :: (a -> b) -> [a] -> [b]
map' f = foldr ((:).f) []
-- by trying another `foldr` based solution, it get this recursion
map'' :: (a -> b) -> [a] -> [b]
map'' f [] = []
map'' f (x:xs) = f x:map'' f xs
-- implement foldl using foldr
-- NOTE : below 'fold's signatures
-- foldl :: (a -> b -> a) -> a -> [b] -> a
-- foldr :: (b -> a -> a) -> a -> [b] -> a
myFoldl :: (a -> b -> a) -> a -> [b] -> a
myFoldl f = foldr (flip f) -- by comparing 'fold's 1st argument signatures
                           -- we need to move from (a -> b -> ...) to (b -> a -> ...)
                           -- wich is flip function
myFoldl' :: (a -> b -> a) -> a -> [b] -> a
myFoldl' f base xs = foldr
  (
    -- accF :: b -> (a -> b) -> a -> b
    -- 1. accV : an accumulated value
    -- 2. accF : an accumulator function (this function, recursive)
    -- 3. a value to change 'accumulated value' accV (1st argument)
    -- NOTE : let aa = ( a-> b), then type of accF is b -> A -> A
    --        wich is the same foldr 1st argument : b -> a -> a
    \accV accF chgE -> accF (f chgE accV)
  )
  id -- to match foldr 2nd argument, here id type is (a -> b) -> (a -> b), ie. return so exact input function
  xs base
-- NOTES : to find accF type
-- f = (\a b -> a) -- :t f == f :: a -> b -> a
-- accF' = (\accV accF' chgE -> accF' (f chgE accV)) -- :t g :: p2 -> (t1 -> t2) -> t1 -> t2
-- accF'' :: b -> (a -> b) -> a -> b
-- accF'' accV accF chgE = accF (f chgE accV)
