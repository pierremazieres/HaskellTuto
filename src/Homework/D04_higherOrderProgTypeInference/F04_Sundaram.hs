module Homework.D04_higherOrderProgTypeInference.F04_Sundaram where
import Test.HUnit
import Data.List
-- single sieve
singleSieve :: Int -> Int -> Int -> Int
singleSieve n j i
    | 1 <= i && i <= j && i+j+(2*i*j) <= n = i+j+(2*i*j)
    | otherwise = 0
testSingleSieve0 :: Test
testSingleSieve0 = TestCase (assertEqual "testSingleSieve0" 0 (singleSieve 10 2 (-1)))
testSingleSieve1 :: Test
testSingleSieve1 = TestCase (assertEqual "testSingleSieve1" 0 (singleSieve 10 1 2))
testSingleSieve2 :: Test
testSingleSieve2 = TestCase (assertEqual "testSingleSieve2" 0 (singleSieve 3 6 4))
testSingleSieve3 :: Test
testSingleSieve3 = TestCase (assertEqual "testSingleSieve3" 67 (singleSieve 100 7 4))
-- lower sieve
lowerSieve :: Int -> Int -> [Int]
lowerSieve n j = Prelude.filter (>0) (Prelude.map (singleSieve n j) [1..j])
testLowerSieve0 :: Test
testLowerSieve0 = TestCase (assertEqual "testLowerSieve0" [16,27,38,49,60] (lowerSieve 100 5))
testLowerSieve1 :: Test
testLowerSieve1 = TestCase (assertEqual "testLowerSieve1" [] (lowerSieve 10 5))
-- upper sieve
sieveSundaram  :: Int -> [Int]
sieveSundaram  n = map (\ x -> (2*x)+1) ([1..n] \\ (Prelude.foldr (\ x y -> x ++ y) [] (Prelude.filter (/=[]) (Prelude.map (lowerSieve n) [1..n]))))
testSieveSundaram0 :: Test
testSieveSundaram0 = TestCase (assertEqual "testSieveSundaram0" [] (sieveSundaram  0))
testSieveSundaram1 :: Test
testSieveSundaram1 = TestCase (assertEqual "testSieveSundaram1" [3,5,7,11,13,17,19,23,29,31] (sieveSundaram  15))
-- concatenate & run all tests
tests :: Test
tests = TestList
    [
     testSingleSieve0,testSingleSieve1,testSingleSieve2,testSingleSieve3
     ,testLowerSieve0,testLowerSieve1
     ,testSieveSundaram0,testSieveSundaram1
    ]
main :: IO Counts
main = runTestTT tests

-- :reload Homework.D04_higherOrderProgTypeInference.F04_Sundaram

l = [1..100]
test0 = filter (/=3) l == [1,2]++[4..100]

filters :: (Eq a)  => [a] -> [a] -> [a]
filters [] l = l
filters (e:[]) l = filter (/=e) l
filters (e:el) l = filters el $ filter (/=e) l
test1 = filters [2,5,7] l == [1,3,4,6]++[8..100]

-- for given i < n, return js
js :: Int -> Int -> [Int]
js = enumFromTo

-- for given i < n , return [i+j+2ij]
addIJs :: Int -> Int -> [Int]
addIJs i n = zipWith (\i j -> i+j+(2*i*j)) (take (n-i+1) (repeat i)) (enumFromTo i n)
-- si i=2 et n=5, alors :
-- j va de 2 à 5 = [2,3,4,5]
-- ij = [4,6,8,10]
-- 2ij = [8,12,16,20]
-- j+2ij = [10,15,20,25]
-- i+j+2ij = [12,17,22,27]
-- addIJs 2 5 == [12,17,22,27]

-- for given i < n , return [i+j+2ij]
addIJs' :: Int -> [[Int]]
addIJs' n = map (\i -> addIJs i n) [1..n]
-- si on prend n = 3
-- alors (addIJs' 3) == [(addIJs 1 3) , (addIJs 2 3) , (addIJs 3 3)]
--       (addIJs' 3) == [[4,7,10] , [12,17] , [24]]

-- for given i < n , return [i+j+2ij]
addIJs'' :: Int -> [Int]
addIJs'' n = foldl (++) [] (addIJs' n)
-- (addIJs'' 3) == [4,7,10,12,17,24]

-- candidates to become primes
candidates' :: Int -> [Int]
--candidates' n = filters (addIJs'' n) [1..n]
candidates' n = filters (cartProd''' n) [1..n]

-- get primes
primes' :: Int -> [Int]
primes' n = map (\n -> 2*n+1) (candidates' n)
-- 2:(primes' 100) == takeWhile (<202) primesRef

-- reference primes for check
-- see : https://stackoverflow.com/questions/3596502/lazy-list-of-prime-numbers
primesRef :: [Int]
primesRef = sieve (2 : [3, 5..])
  where
    sieve (p:xs) = p : sieve [x|x <- xs, x `mod` p > 0]

cartProd :: [a] -> [b] -> [(a, b)]
cartProd xs ys = [(x,y) | x <- xs, y <- ys]

cartProd' :: [a] -> [(a, a)]
cartProd' l = [(n,m) | n <- l, m <- l]

cartProd'' :: (Num a) => [a] -> [a]
cartProd'' l = [n+m+2*n*m | n <- l, m <- l]

cartProd''' :: (Num a,Enum a)  => a -> [a]
cartProd''' l = [n+m+2*n*m | n <- [1..l], m <- [1..l]]
