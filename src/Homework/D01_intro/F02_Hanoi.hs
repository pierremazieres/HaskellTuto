module Homework.D01_intro.F02_Hanoi where
-- specific types
type Peg = String
type Move = (Peg, Peg)
-- solve hanoi tower
-- x            x
-- X     ->     X
-- a b c    a b c
--p :: [Move]
--p = [(a,c)]
hanoi3 :: Integer -> Peg -> Peg -> Peg -> [Move]
--p = [(a,c)]
hanoi3 n a b c
  | n <= 0 = []
  | n == 1 = p
  | otherwise =
    let m = n-1
    in hanoi3 m a c b ++ p ++ hanoi3 m b a c
  where p = [(a,c)]
hanoi4 :: Integer -> Peg -> Peg -> Peg -> Peg -> [Move]
hanoi4 n a b c d
  | n <= 0 = []
  | otherwise =
    let k = n - round(sqrt(2*fromIntegral(n)+1)) + 1
    in hanoi4 k a c d b ++ hanoi3 (n-k) a c d ++ hanoi4 k b a c d
