module Homework.D01_intro.F01_ValidatingCreditCardNumbers where
-- toDigits should convert positive Integers to a list of digits
-- For 0 or negative inputs,toDigits should return the empty list
toDigits :: Integer -> [Integer]
toDigits n
  | n==0 = []
  | otherwise = toDigits(div n 10) ++ [mod n 10]
-- toDigitsRev should do the same, but with the digits reversed
toDigitsRev :: Integer -> [Integer]
toDigitsRev n = reverse $ toDigits n
-- Once we have the digits in the proper order, we need to double every other one
doubleEveryOther :: [Integer] -> [Integer]
doubleEveryOther [] = []
doubleEveryOther (n:[]) = n:[]
doubleEveryOther l =
  let tl = (length l)-2
      first = take tl l
      last2 = drop tl l
      second2last = 2 * (head last2)
      veryLast = last last2
  in doubleEveryOther first ++ [second2last,veryLast]
-- calculate the sum of all digits
sumDigits :: [Integer] -> Integer
sumDigits [] = 0
sumDigits (n:sl) = sum (toDigits n) + sumDigits sl
-- indicates whether an Integer could be a valid credit card number
validate :: Integer -> Bool
validate n = mod ( sumDigits $ doubleEveryOther.toDigits $ n ) 10 == 0
