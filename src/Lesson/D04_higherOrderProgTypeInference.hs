module Lesson.D04_higherOrderProgTypeInference where
-- Anonymous functions
gt100 :: Integer -> Bool
gt100 x = x > 100
greaterThan100 :: [Integer] -> [Integer]
greaterThan100 xs = filter gt100 xs
greaterThan100_2 :: [Integer] -> [Integer]
greaterThan100_2 xs = filter (\x -> x > 100) xs
greaterThan100_3 :: [Integer] -> [Integer]
greaterThan100_3 xs = filter (>100) xs
-- Function composition
foo :: (b -> c) -> (a -> b) -> (a -> c)
foo f g = \x -> f (g x)
myTest :: [Integer] -> Bool
myTest xs = even (length (greaterThan100 xs))
myTest' :: [Integer] -> Bool
myTest' = even . length . greaterThan100
-- Currying and partial application
f :: Int -> Int -> Int
f x y = 2*x + y
f' :: Int -> (Int -> Int)
f' x y = 2*x + y
comp :: (b -> c) -> (a -> b) -> a -> c
comp f g x = f (g x)
f'' :: (Int,Int) -> Int
f'' (x,y) = 2*x + y
schonfinkel :: ((a,b) -> c) -> a -> b -> c
schonfinkel f x y = f (x,y)
unschonfinkel :: (a -> b -> c) -> (a,b) -> c
unschonfinkel f (x,y) = f x y
-- Wholemeal programming
foobar :: [Integer] -> Integer
foobar []     = 0
foobar (x:xs)
  | x > 3     = (7*x + 2) + foobar xs
  | otherwise = foobar xs
foobar' :: [Integer] -> Integer
foobar' = sum . map (\x -> 7*x + 2) . filter (>3)
-- Folds
sum' :: [Integer] -> Integer
sum' []     = 0
sum' (x:xs) = x + sum' xs
product' :: [Integer] -> Integer
product' [] = 1
product' (x:xs) = x * product' xs
length' :: [a] -> Int
length' []     = 0
length' (_:xs) = 1 + length' xs
fold :: b -> (a -> b -> b) -> [a] -> b
fold z f []     = z
fold z f (x:xs) = f x (fold z f xs)
sum''     = fold 0 (+)
product'' = fold 1 (*)
length''  = fold 0 (\_ s -> 1 + s)
-- *****RUN
mainD04 :: IO ()
mainD04 = do {
  print (greaterThan100 [1,9,349,6,907,98,105])
  ; print (greaterThan100_2 [1,9,349,6,907,98,105])
  ; print ((\x y z -> [x,2*y,3*z]) 5 6 3)
  ; print (greaterThan100_3 [1,9,349,6,907,98,105])
  ; print ((>100) 102)
  ; print ((100>) 102)
  ; print (map (*6) [1..5])
  ; print (myTest [1,9,349,6,907,98,105])
  ; print (myTest' [1,9,349,6,907,98,105])
  ; print (f 1 9)
  ; print (f' 1 9)
  ; print (comp (\x -> x**2) (\x -> log(x)) 5)
  ; print (f'' (1,9))
  ; print (schonfinkel f'' 1 9)
  ; print (unschonfinkel f (1,9))
  ; print (uncurry (+) (2,3))
  ; print (foobar [1,9,349,6,907,98,105])
  ; print (foobar' [1,9,349,6,907,98,105])
  ; print (sum' [1,9,349,6,907,98,105])
  ; print (product' [1,9,349,6,907,98,105])
  ; print (length' [1,9,349,6,907,98,105])
  ; print (sum'' [1,9,349,6,907,98,105])
  ; print (product'' [1,9,349,6,907,98,105])
  ; print (length'' [1,9,349,6,907,98,105])
}
 