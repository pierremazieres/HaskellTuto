module Lesson.D03_RecursionPatternPolyPrelude where
-- Recursion patterns
data IntList = Empty | Cons Int IntList
    deriving Show
absAll :: IntList -> IntList
absAll Empty       = Empty
absAll (Cons x xs) = Cons (abs x) (absAll xs)
squareAll :: IntList -> IntList
squareAll Empty       = Empty
squareAll (Cons x xs) = Cons (x*x) (squareAll xs)
mapIntList :: (Int -> Int) -> IntList -> IntList
mapIntList f (Cons i list) = Cons (f i) (mapIntList f list)
mapIntList _ _ = Empty
filterIntList :: (Int -> Bool) -> IntList -> IntList
filterIntList f (Cons i list) = case f i of
    True  -> Cons i filteredSubList
    False -> filteredSubList
    where filteredSubList = filterIntList f list
filterIntList _ _ = Empty
-- Polymorphism
data List t = E | C t (List t)
    deriving Show
filterList :: (t -> Bool) -> List t -> List t
filterList _ E = E
filterList p (C x xs)
  | p x       = C x (filterList p xs)
  | otherwise = filterList p xs
mapList :: (a -> b) -> List a -> List b
mapList f (C x list) = C (f x) (mapList f list)
mapList _ _ = E
-- Total and partial functions
doStuff1  :: [Int] -> Int
doStuff1  []  = 0
doStuff1  [_] = 0
doStuff1  xs  = head xs + (head (tail xs)) 
doStuff2 :: [Int] -> Int
doStuff2 []        = 0
doStuff2 [_]       = 0
doStuff2 (x1:x2:_) = x1 + x2
safeHead :: [a] -> Maybe a
safeHead []    = Nothing
safeHead (x:_) = Just x
data NonEmptyList a = NEL a [a]
    deriving Show
nelToList :: NonEmptyList a -> [a]
nelToList (NEL x xs) = x:xs
listToNel :: [a] -> Maybe (NonEmptyList a)
listToNel []     = Nothing
listToNel (x:xs) = Just $ NEL x xs
headNEL :: NonEmptyList a -> a
headNEL (NEL a _) = a
tailNEL :: NonEmptyList a -> [a]
tailNEL (NEL _ as) = as
-- *****RUN
exampleIntList = Cons (-1) (Cons 2 (Cons (-6) Empty))
addOne x = x + 1
square x = x * x
isEven_ x =     mod x 2 == 0
isNegative x = x < 0
isPositive x = x > 0
exampleList = C (-1) (C 2 (C (-6) E))
lst1 :: List Int
lst1 = C 3 (C 5 (C 2 E))
lst2 :: List Char
lst2 = C 'x' (C 'y' (C 'z' E))
lst3 :: List Bool
lst3 = C True (C False E)
-- *****RUN
mainD03 :: IO ()
mainD03 = do {
  print (absAll    exampleIntList);
  print (squareAll exampleIntList);
  print (mapIntList abs    exampleIntList);
  print (mapIntList addOne exampleIntList);
  print (mapIntList square exampleIntList);
  print (filterIntList isEven_     exampleIntList);
  print (filterIntList isNegative exampleIntList);
  print (filterIntList isPositive exampleIntList);
  print (filterList isEven_     exampleList);
  print (filterList isNegative exampleList);
  print (filterList isPositive exampleList);
  print (mapList abs lst1);
  print (mapList addOne lst1);
  print (mapList square lst1);
  --print (head []); -- CRASH: head unsafe and can not handle empty list
  print (head [0]);
  --print (tail []); -- CRASH: tail unsafe and can not handle empty list
  print (tail [0,1]);
  print (map doStuff1 [[0],[1,2],[3,4,5]], map doStuff2 [[0],[1,2],[3,4,5]], map doStuff1 [[0],[1,2],[3,4,5]]==map doStuff2 [[0],[1,2],[3,4,5]]);
  print (safeHead ([]::[Int]));
  print (safeHead [0]);
  print (nelToList (NEL 1 [2,3]));
  print (listToNel ([]::[Int]));
  print (listToNel [1,2,3]);
  print (headNEL (NEL 1 [2,3]));
  print (tailNEL (NEL 1 [2,3]));
}