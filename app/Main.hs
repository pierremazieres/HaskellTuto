module Main where
import Lesson.D01_intro
import Lesson.D02_AlgebraicDataTypes
import Lesson.D03_RecursionPatternPolyPrelude
import Lesson.D04_higherOrderProgTypeInference
main :: IO ()
main = do {
  mainD01;
  mainD02;
  mainD03;
  mainD04;
}
