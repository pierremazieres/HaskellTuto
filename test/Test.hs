-- for 'build --test' command to run :
-- 1. file name must not be Main
-- 2. module must Main
module Main where
import Homework.D01_intro.T01_ValidatingCreditCardNumbers
import Homework.D01_intro.T02_Hanoi
import Homework.D02_AlgebraicDataTypes.TestLogAnalysis
import Homework.D03_RecursionPatternPolyPrelude.TGolf
import Homework.D04_higherOrderProgTypeInference.T01_Wholemeal
import Homework.D04_higherOrderProgTypeInference.T02_FoldingTrees
import Homework.D04_higherOrderProgTypeInference.T03_MoreFolds
import Test.HUnit
main :: IO Counts
main = do {
  mainD01T01;
  mainD01T02;
  mainTestLogAnalysis;
  mainTGolf;
  mainD04T01;
  mainD04T02;
  mainD04T03;
}