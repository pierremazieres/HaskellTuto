module Homework.D03_RecursionPatternPolyPrelude.TGolf where
import Homework.D03_RecursionPatternPolyPrelude.Golf
import Test.HUnit
skip0 :: Test
skip0 = TestCase $ assertEqual "skip0" "ABCD" $ skip "ABCD" 1
skip1 :: Test
skip1 = TestCase $ assertEqual "skip1" "BD" $ skip "ABCD" 2
skip2 :: Test
skip2 = TestCase $ assertEqual "skip2" "C" $ skip "ABCD" 3
skip3 :: Test
skip3 = TestCase $ assertEqual "skip3" "D" $ skip "ABCD" 4
skip4 :: Test
skip4 = TestCase $ assertEqual "skip4" "" $ skip "ABCD" 5
skips0 :: Test
skips0 = TestCase $ assertEqual "skips0" ["ABCD", "BD", "C", "D"] $ skips "ABCD"
skips1 :: Test
skips1 = TestCase $ assertEqual "skips1" ["hello!", "el!", "l!", "l", "o", "!"] $ skips "hello!"
skips2 :: Test
skips2 = TestCase $ assertEqual "skips2" [[1]] $ skips [1]
skips3 :: Test
skips3 = TestCase $ assertEqual "skips3" [[True,False], [False]] $ skips [True,False]
skips4 :: Test
skips4 = TestCase $ assertEqual "skips4" ([]::[[Int]]) $ skips ([]::[Int])
localMaximum0 :: Test
localMaximum0 = TestCase $ assertEqual "localMaximum0" Nothing $ localMaximum (1,2,3)
localMaximum1 :: Test
localMaximum1 = TestCase $ assertEqual "localMaximum1" (Just 3) $ localMaximum (2,3,1)
triplets0 :: Test
triplets0 = TestCase $ assertEqual "triplets0" ([]::[(Integer, Integer, Integer)]) $ triplets ([]::[Integer])
triplets1 :: Test
triplets1 = TestCase $ assertEqual "triplets1" ([]::[(Integer, Integer, Integer)]) $ triplets [1]
triplets2 :: Test
triplets2 = TestCase $ assertEqual "triplets2" ([]::[(Integer, Integer, Integer)]) $ triplets [1,2]
triplets3 :: Test
triplets3 = TestCase $ assertEqual "triplets3" [(1,2,3)] $ triplets [1,2,3]
triplets4 :: Test
triplets4 = TestCase $ assertEqual "triplets4" [(1,2,3),(2,3,4)] $ triplets [1,2,3,4]
localMaxima0 :: Test
localMaxima0 = TestCase $ assertEqual "localMaxima0" [9,6] $ localMaxima [2,9,5,6,1]
localMaxima1 :: Test
localMaxima1 = TestCase $ assertEqual "localMaxima1" [4] $ localMaxima [2,3,4,1,5]
localMaxima2 :: Test
localMaxima2 = TestCase $ assertEqual "localMaxima2" ([]::[Integer]) $ localMaxima [1,2,3,4,5]
printLine0 :: Test
printLine0 = TestCase $ assertEqual "printLine0" "      ****\n" $ printLine (0,1,2,3,4,5,6,7,8,9) 5
ceil0 :: Test
ceil0 = TestCase $ assertEqual "ceil0" 3 $ ceil (0,1,2,3,4,1,2,0,0,1)
printHistogram0 :: Test
printHistogram0 = TestCase $ assertEqual "printHistogram0" "         *\n        **\n       ***\n      ****\n     *****\n    ******\n   *******\n  ********\n *********\n=========\n123456789\n" $ printHistogram (0,1,2,3,4,5,6,7,8,9)
count0 :: Test
count0 = TestCase $ assertEqual "count0" 2 $ count [1,4,5,4,6,6,3,4,2,4,9] 6
toHistogram0 :: Test
toHistogram0 = TestCase $ assertEqual "toHistogram0" (0,1,1,1,4,1,2,0,0,1) $ toHistogram [1,4,5,4,6,6,3,4,2,4,9]
histogram0 :: Test
histogram0 = TestCase $ assertEqual "histogram0" " *        \n *        \n *   *    \n=========\n123456789\n" $ histogram [1,1,1,5]
histogram1 :: Test
histogram1 = TestCase $ assertEqual "histogram1" "    *     \n    *     \n    * *   \n ******  *\n=========\n123456789\n" $ histogram [1,4,5,4,6,6,3,4,2,4,9]
-- concatenate & run all tests
tests :: Test
tests = TestList [
  skip0 ,skip1 ,skip2 ,skip3 ,skip4, skips0 ,skips1 ,skips2 ,skips3 ,skips4, localMaximum0, localMaximum1, triplets0
  , triplets1, triplets2, triplets3, triplets4, localMaxima0, localMaxima1, localMaxima2, printLine0, ceil0, count0
  , printHistogram0, toHistogram0, histogram0, histogram1
 ]
mainTGolf :: IO Counts
mainTGolf = runTestTT tests
