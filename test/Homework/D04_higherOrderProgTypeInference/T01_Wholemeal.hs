module Homework.D04_higherOrderProgTypeInference.T01_Wholemeal where
import Homework.D04_higherOrderProgTypeInference.F01_Wholemeal
import Test.HUnit
testFun1 :: Test
testFun1 = TestCase $ assertEqual "testFun1" (map fun1 lists) (map fun1' lists)
  where lists = [[],[0],[1],[2],[3],[0..1],[0..2],[3..5],[3..7],[3..10]]
testFun2 :: Test
testFun2 = TestCase $ assertEqual "testFun2" (map fun2 list) $ map fun2' list
  where list = [1..100]
-- concatenate & run all tests
tests :: Test
tests = TestList [testFun1,testFun2]
mainD04T01 :: IO Counts
mainD04T01 = runTestTT tests
