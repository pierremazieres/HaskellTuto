module Homework.D04_higherOrderProgTypeInference.T03_MoreFolds where
import Homework.D04_higherOrderProgTypeInference.F03_MoreFolds
import Test.HUnit
-- xor
testXor0 :: Test
testXor0 = TestCase $ assertBool "testXor0" $ not.xor $ [False, False]
testXor1 :: Test
testXor1 = TestCase $ assertBool "testXor1" $ xor [True, False]
testXor2 :: Test
testXor2 = TestCase $ assertBool "testXor2" $ xor [False, True]
testXor3 :: Test
testXor3 = TestCase $ assertBool "testXor3" $ not.xor $ [True, True]
testXor4 :: Test
testXor4 = TestCase $ assertBool "testXor4" $ xor [False, True, False]
testXor5 :: Test
testXor5 = TestCase $ assertBool "testXor5" $ not.xor $ [False, True, False, False, True]
testFunction = (<0)
testInput = [-1,1]
expectedList = map testFunction testInput
testMap' :: Test
testMap' = TestCase $ assertEqual "testMap'" expectedList $ map' testFunction testInput
testMap'' :: Test
testMap'' = TestCase $ assertEqual "testMap''" expectedList $ map'' testFunction testInput
foldList = [2,3,4]
foldAcc = 1
expectedFoldL = foldl (^) foldAcc foldList
testMyFoldl :: Test
testMyFoldl = TestCase $ assertEqual "testMyFoldl" expectedFoldL $ myFoldl (^) foldAcc foldList
testMyFoldl' :: Test
testMyFoldl' = TestCase $ assertEqual "testMyFoldl'" expectedFoldL $ myFoldl' (^) foldAcc foldList
-- concatenate & run all tests
tests :: Test
tests = TestList
    [
     testXor0,testXor1,testXor2,testXor3,testXor4,testXor5,
     testMap',testMap'',testMyFoldl,testMyFoldl'
    ]
mainD04T03 :: IO Counts
mainD04T03 = runTestTT tests
