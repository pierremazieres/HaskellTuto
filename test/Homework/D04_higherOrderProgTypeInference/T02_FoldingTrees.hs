module Homework.D04_higherOrderProgTypeInference.T02_FoldingTrees where
import Homework.D04_higherOrderProgTypeInference.F02_FoldingTrees
import Test.HUnit
leaf = Leaf::Tree Char
e = 'a'
tree0 = Node 0 Leaf e Leaf
tree1 = Node 1 Leaf e tree0
tree2 = Node 1 tree0 e Leaf
tree3 = Node 1 tree0 e tree0
balanced0 :: Test
balanced0 = TestCase $ assertBool "balanced0" $ balanced leaf
balanced1 :: Test
balanced1 = TestCase $ assertBool "balanced1" $ balanced tree0
balanced2 :: Test
balanced2 = TestCase $ assertBool "balanced2" $ balanced tree1
balanced3 :: Test
balanced3 = TestCase $ assertBool "balanced3" $ balanced tree2
balanced4 :: Test
balanced4 = TestCase $ assertBool "balanced4" $ balanced tree3
shortestHeight0 :: Test
shortestHeight0 = TestCase $ assertEqual "shortestHeight0" 0 $ shortestHeight leaf
shortestHeight1 :: Test
shortestHeight1 = TestCase $ assertEqual "shortestHeight1" 1 $ shortestHeight tree0
fullyFilled0 :: Test
fullyFilled0 = TestCase $ assertBool "fullyFilled0" $ fullyFilled leaf
fullyFilled1 :: Test
fullyFilled1 = TestCase $ assertBool "fullyFilled1" $ fullyFilled tree0
fullyFilled2 :: Test
fullyFilled2 = TestCase $ assertBool "fullyFilled2" $ not.fullyFilled $ tree1
fullyFilled3 :: Test
fullyFilled3 = TestCase $ assertBool "fullyFilled3" $ not.fullyFilled $ tree2
fullyFilled4 :: Test
fullyFilled4 = TestCase $ assertBool "fullyFilled4" $ not.fullyFilled $ tree3
addElem0 :: Test
addElem0 = TestCase $ assertBool "addElem0" $ balanced $ addElem e Leaf
addElem1 :: Test
addElem1 = TestCase $ assertBool "addElem1" $ balanced $ addElem e tree0
addElem2 :: Test
addElem2 = TestCase $ assertBool "addElem2" $ balanced $ addElem e tree1
addElem3 :: Test
addElem3 = TestCase $ assertBool "addElem3" $ balanced $ addElem e tree2
addElem4 :: Test
addElem4 = TestCase $ assertBool "addElem4" $ balanced $ addElem e tree3
inputTree = Node 2 tree3 e tree1
expectedTree = Node 2 tree3 e tree3
addElem5 :: Test
addElem5 = TestCase $ assertEqual "addElem5" expectedTree $ addElem e inputTree
foldTree0 :: Test
foldTree0 = TestCase $ assertEqual "foldTree0" leaf $ foldTree []
foldTree1 :: Test
foldTree1 = TestCase $ assertEqual "foldTree1" tree0 $ foldTree "a"
finalTree = (
    Node 3
    (Node 1 (Node 0 Leaf 'I' Leaf) 'G' (Node 0 Leaf 'H' Leaf))
    'A'
    ((Node 2 (Node 1 (Node 0 Leaf 'F' Leaf) 'D' (Node 0 Leaf 'E' Leaf))) 'B' (Node 1 Leaf 'C' (Node 0 Leaf 'J' Leaf)))
  )
--      A
--    /    \
--   G      B
-- /  \    /  \
-- I  H    D   C
--       /  \  |
--       F   E J
foldedTree = foldTree "ABCDEFGHIJ"
foldTree2 :: Test
foldTree2 = TestCase $ assertEqual "foldTree2" finalTree foldedTree
foldTree2' :: Test
foldTree2' = TestCase $ assertBool "foldTree2'" $ balanced foldedTree

-- concatenate & run all tests
tests :: Test
tests = TestList [
    balanced0,balanced1,balanced2,balanced3,balanced4,shortestHeight0,shortestHeight1,
    fullyFilled0,fullyFilled1,fullyFilled2,fullyFilled3,fullyFilled4,
    addElem0,addElem1,addElem2,addElem3,addElem4,addElem5,foldTree0,foldTree1,foldTree2,foldTree2'
  ]
mainD04T02 :: IO Counts
mainD04T02 = runTestTT tests
