module Homework.D02_AlgebraicDataTypes.TestLogAnalysis where
import Homework.D02_AlgebraicDataTypes.LogAnalysis
import Homework.D02_AlgebraicDataTypes.Log
import Test.HUnit
txtMessage = "test"
numLogLevel = 0
txtLogLevel = show numLogLevel
logMessage0 = LogMessage Info numLogLevel txtMessage
logMessage1 = LogMessage Info (numLogLevel+1) "__"
unknownLogMessage = Unknown txtMessage
unknownLogMessages = [unknownLogMessage]
messagesList0 = [logMessage0]
messagesList1 = messagesList0++[logMessage1]
messageTree0 = Node Leaf logMessage0 Leaf
messageTree1 = Node messageTree0 logMessage1 Leaf
messageTree2 = Node Leaf logMessage1 Leaf
parseMessage0 :: Test
parseMessage0 = TestCase $ assertEqual "parseMessage0" (LogMessage (Error numErrorLevel) numLogLevel txtMessage) $ parseMessage $ unwords ["E",txtErrorLevel,txtLogLevel,txtMessage]
  where
    numErrorLevel = numLogLevel+1
    txtErrorLevel = show numErrorLevel
parseMessage1 :: Test
parseMessage1 = TestCase $ assertEqual "parseMessage1" logMessage0 $ parseMessage $ unwords ["I",txtLogLevel,txtMessage]
parseMessage2 :: Test
parseMessage2 = TestCase $ assertEqual "parseMessage2" unknownLogMessage $ parseMessage txtMessage
parseMessage3 :: Test
parseMessage3 = TestCase $ assertEqual "parseMessage3" (LogMessage Warning numLogLevel txtMessage) $ parseMessage $ unwords ["W",txtLogLevel,txtMessage]
parse0 :: Test
parse0 = TestCase $ assertEqual "parse0" [] $ parse ""
parse1 :: Test
parse1 = TestCase $ assertEqual "parse1" unknownLogMessages $ parse txtMessage
insert0 :: Test
insert0 = TestCase $ assertEqual "insert0" messageTree0 $ insert logMessage0 Leaf
insert1 :: Test
insert1 = TestCase $ assertEqual "insert1" (Node Leaf logMessage0 messageTree2) $ insert logMessage1 messageTree0
insert2 :: Test
insert2 = TestCase $ assertEqual "insert2" messageTree1 $ insert logMessage0 messageTree2
insert3 :: Test
insert3 = TestCase $ assertEqual "insert3" Leaf $ insert unknownLogMessage messageTree0
build0 :: Test
build0 = TestCase $ assertEqual "build0" messageTree0 $ build messagesList0
build1 :: Test
build1 = TestCase $ assertEqual "build1" Leaf $ build []
inOrder0 :: Test
inOrder0 = TestCase $ assertEqual "inOrder0" messagesList0 $ inOrder messageTree0
inOrder1 :: Test
inOrder1 = TestCase $ assertEqual "inOrder1" messagesList1 $ inOrder messageTree1
inOrder2 :: Test
inOrder2 = TestCase $ assertEqual "inOrder2" messagesList1 $ inOrder $ build $ reverse messagesList1
inOrder3 :: Test
inOrder3 = TestCase $ assertEqual "inOrder3" [] $ inOrder $ Node Leaf unknownLogMessage Leaf
whatWentWrong0 :: Test
whatWentWrong0 = TestCase $ assertEqual "whatWentWrong0" [txtMessage] $ whatWentWrong [LogMessage (Error relevantErrorLevel) numLogLevel txtMessage]
whatWentWrong1 :: Test
whatWentWrong1 = TestCase $ assertEqual "whatWentWrong1" [] $ whatWentWrong [LogMessage (Error $ relevantErrorLevel-1) numLogLevel txtMessage]
whatWentWrong2 :: Test
whatWentWrong2 = TestCase $ assertEqual "whatWentWrong2" [] $ whatWentWrong unknownLogMessages
-- concatenate & run all tests
tests :: Test
tests = TestList [
  parseMessage0, parseMessage1, parseMessage2, parseMessage3, parse0, parse1,
  insert0, insert1, insert2, insert3, build0, build1, inOrder0, inOrder1, inOrder2, inOrder3,
  whatWentWrong0, whatWentWrong1, whatWentWrong2
 ]
mainTestLogAnalysis :: IO Counts
mainTestLogAnalysis = runTestTT tests
-- testParse parse 10 "src/Homework/D02_AlgebraicDataTypes/error.log"
