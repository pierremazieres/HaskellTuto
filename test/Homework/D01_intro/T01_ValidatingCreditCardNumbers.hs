module Homework.D01_intro.T01_ValidatingCreditCardNumbers where
import Homework.D01_intro.F01_ValidatingCreditCardNumbers
import Test.HUnit
testToDigits0 :: Test
testToDigits0 = TestCase $ assertEqual "testToDigits0" [] $ toDigits 0
testToDigits1 :: Test
testToDigits1 = TestCase $ assertEqual "testToDigits1" [1,2,3,4] $ toDigits 1234
testToDigitsRev :: Test
testToDigitsRev = TestCase $ assertEqual "testToDigitsRev" [4,3,2,1] $ toDigitsRev 1234
testDoubleEveryOther0 :: Test
testDoubleEveryOther0 = TestCase $ assertEqual "testDoubleEveryOther0" [16,7,12,5] $ doubleEveryOther [8,7,6,5]
testDoubleEveryOther1 :: Test
testDoubleEveryOther1 = TestCase $ assertEqual "testDoubleEveryOther1" [1,4,3] $ doubleEveryOther [1,2,3]
testSumDigits :: Test
testSumDigits = TestCase $ assertEqual "testSumDigits" 22 $ sumDigits [16,7,12,5]
testValidate0 :: Test
testValidate0 = TestCase $ assertBool "testValidate0" $ validate 4012888888881881
testValidate1 :: Test
testValidate1 = TestCase $ assertBool "testValidate1" $ not.validate $ 4012888888881882
-- concatenate & run all tests
tests :: Test
tests = TestList [
  testToDigits0, testToDigits1, testToDigitsRev
  , testDoubleEveryOther0 , testDoubleEveryOther1, testSumDigits, testValidate0, testValidate1
 ]
mainD01T01 :: IO Counts
mainD01T01 = runTestTT tests
