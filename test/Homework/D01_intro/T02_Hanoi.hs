module Homework.D01_intro.T02_Hanoi where
import Homework.D01_intro.F02_Hanoi
import Test.HUnit
testHanoi30 :: Test
testHanoi30 = TestCase $ assertEqual "testHanoi30" [("a","b"),("a","c"),("b","c")] $ hanoi3 2 "a" "b" "c"
testHanoi31 :: Test
testHanoi31 = TestCase $ assertEqual "testHanoi31" 32767 $ length $ hanoi3 15 "a" "b" "c"
testHanoi32 :: Test
testHanoi32 = TestCase $ assertEqual "testHanoi32" [] $ hanoi3 0 "" "" ""
testHanoi4 :: Test
testHanoi4 = TestCase $ assertEqual "testHanoi4" 129 $ length $ hanoi4 15 "a" "b" "c" "d"
-- concatenate & run all tests
tests :: Test
tests = TestList [
  testHanoi30, testHanoi31, testHanoi32, testHanoi4
 ]
mainD01T02 :: IO Counts
mainD01T02 = runTestTT tests
